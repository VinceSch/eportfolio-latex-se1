1. LaTeX:

    Von LaTeX gibt es enorm viele Distributionen, empfehlen kann ich MiKTeX und TeXMaker.
    Man benötigt beide Programme und es ist wichtig in welcher Reihenfolge sie installiert werden.

    Bitte ZUERST MiKTeX und ANSCHLIEßEND TeXMaker installieren, da es sonst zu ungewollten Komplikationen kommen kann.



2. Installation:

    Packages sind wichtig, also bei der Installation von MiKTeX bitte angeben, dass die Packages entweder automatisch installieren werden oder auf Nachfrage.
    Bei der Installation vom TeXMaker muss nichts beachtet werden.
 
 
 
3. Dateien:

    Die aktuellste Version (2.9.7442) von MiKTeX findet man unter:
    https://miktex.org/download
    
    Die aktuellste Version (5.0.4) von TeXMaker findet man unter:
    https://www.xm1math.net/texmaker/download.html